import React from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    ScrollView,
    Image
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import getTypeColor from '../Types'
import getStatsColor from '../Stats'
import getHabitatImage from '../Habitat'

const { width, height } = Dimensions.get('window');

const Loading = () => (
    <View>
        <Text style={[styles.pokeDescription, { color: '#f05454', fontSize: 18 }]}>... Loading</Text>
    </View>
)

const Detail = ({ route, navigation }) => {
    const { data } = route.params;
    const [evoData, setEvoData] = React.useState([])

    React.useEffect(() => {
        async function fetchSpecies() {
            fetch(`https://pokeapi.co/api/v2/pokemon-species/${data.id}/`)
                .then(response => response.json())
                .then(json => {
                    // console.log(json)
                    setEvoData(json)
                    return json
                })
            // fetchChain()
        }

        fetchSpecies()
    }, [])

    async function fetchChain() {
        fetch(evoData.evolution_chain.url)
            .then(response => response.json())
            .then(json => {
                console.log(json.chain.evolves_to)
            })
    }

    if (evoData.length === 0) {
        return <Loading />
    }

    return (
        <View style={styles.container}>

            <View style={styles.upperContainer}>
                <Text style={styles.pokeLetter}>COMPASSO UOL</Text>

                <View style={{ width: '100%', paddingLeft: 8 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                    >
                        <Icon
                            name="chevron-left"
                            size={30}
                            color="#eaebed"
                        />
                    </TouchableOpacity>
                </View>
                <Text style={styles.pokeName}>#{data.id} {data.name}</Text>
                <Text style={styles.pokeDescription}>{evoData.flavor_text_entries[0].flavor_text}</Text>
                {evoData.is_legendary && <Text>Lendario</Text>}
                <Text style={{ fontSize: 8, fontFamily: 'PokeClassic-Regular', position: 'absolute', bottom: 0, right: 0 }}>{evoData.generation.name.toUpperCase()}</Text>
            </View>

            <View style={styles.imageContainer}>
                <Image
                    source={{ uri: data.sprites.front_default }}
                    style={{ width: '100%', height: '100%', resizeMode: 'cover' }}
                />
            </View>

            <ScrollView>
                <View style={styles.bottomContainer}>

                    {evoData.habitat == null ? <Text></Text> :
                        <View style={{ flexDirection: 'row', position: 'absolute', right: 10, top: 10, alignItems: 'center' }}>
                            <Image
                                source={getHabitatImage(evoData.habitat.name)}
                                style={{ width: 30, height: 30, marginRight: 4 }}
                            />
                            <Text style={{ fontFamily: 'PokeClassic-Regular', fontSize: 6 }}>{evoData.habitat.name}</Text>
                        </View>
                    }
                    <View style={{
                        flexDirection: 'row',
                        width: '100%',
                        justifyContent: 'space-around',
                        alignItems: 'center'
                    }}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: '30%'
                        }}>
                            <Text style={{ fontFamily: 'PokeClassic-Regular', fontSize: 12, textAlign: 'center' }}>{(data.height * 0.1).toFixed(2)} m</Text>
                            <Text style={{
                                textAlign: 'center',
                                fontSize: 10,
                                fontFamily: 'PokeClassic-Regular'
                            }}
                            >
                                Height
                        </Text>
                        </View>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: '32%'
                        }}>
                            <Text style={{ fontFamily: 'PokeClassic-Regular', fontSize: 12, textAlign: 'center' }}>{(data.weight * 0.1).toFixed(2)} kg</Text>
                            <Text style={{
                                textAlign: 'center',
                                fontSize: 10,
                                fontFamily: 'PokeClassic-Regular'
                            }}
                            >
                                Weight
                        </Text>
                        </View>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: '30%'
                        }}>
                            <Text style={{ fontFamily: 'PokeClassic-Regular', fontSize: 12, textAlign: 'center' }}>{data.base_experience}</Text>
                            <Text style={{
                                textAlign: 'center',
                                fontSize: 10,
                                fontFamily: 'PokeClassic-Regular'
                            }}
                            >Base XP</Text>
                        </View>
                    </View>

                    {/* TYPES CONTAINER */}
                    <View style={styles.typesContainer}>
                        {data.types.map(type =>
                            <View style={[styles.pokeType, { backgroundColor: getTypeColor(type.type.name) }]}>
                                <Text style={{ fontFamily: 'PokeClassic-Regular', color: 'black' }}>{type.type.name}</Text>
                            </View>
                        )}
                    </View>

                    {/* STATS CONTAINER */}
                    <View style={{ backgroundColor: '#fff', padding: 20, borderRadius: 12, marginVertical: 10, flexDirection: 'column', justifyContent: 'space-between' }} elevation={5}>
                        {data.stats.map(stat =>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 4 }}>
                                <View style={{ flex: 3 }}>
                                    <Text style={{ fontFamily: 'PokeClassic-Regular', color: 'black', fontSize: 8 }}>{stat.stat.name}</Text>
                                </View>
                                <View style={{ flex: 3 }}>
                                    <View style={{ width: `${stat.base_stat}%`, height: 20, borderRadius: 4, backgroundColor: getStatsColor(stat.stat.name) }} />
                                </View>
                                <Text style={{ flex: 1, textAlign: 'right', fontFamily: 'PokeClassic-Regular', fontSize: 8 }}>{stat.base_stat}</Text>
                            </View>
                        )}
                    </View>

                    {/* SHINY CONTAINER */}
                    <View style={{ backgroundColor: '#fff', padding: 10, borderRadius: 12, marginVertical: 20 }} elevation={5}>
                        <Text style={{ fontFamily: 'PokeClassic-Regular', fontSize: 12, textDecorationLine: 'underline' }}>Shiny Version</Text>
                        <View style={styles.shinyContainer}>
                            <Image
                                source={{ uri: data.sprites.front_shiny }}
                                style={{ width: '40%', height: 150, resizeMode: 'contain' }}
                            />
                            <Image
                                source={{ uri: data.sprites.back_shiny }}
                                style={{ width: '40%', height: 150, resizeMode: 'contain' }}
                            />
                        </View>
                    </View>

                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    upperContainer: {
        backgroundColor: '#f05454',
        height: height * .4,
        width: width,
        borderBottomColor: '#222831',
        borderBottomWidth: 6,
        // justifyContent: 'center',
        alignItems: 'center',
    },
    bottomContainer: {
        backgroundColor: '#eaebed',
        width: width,
        flex: 2,
        elevation: 0,
        paddingTop: 70,
        paddingHorizontal: 20
    },
    imageContainer: {
        borderColor: '#222831',
        borderWidth: 4,
        width: 140,
        height: 140,
        borderRadius: 70,
        position: 'absolute',
        top: (height * .4) - 70,
        elevation: 10,
        left: width * .2,
        backgroundColor: '#fff'
    },
    pokeLetter: {
        fontFamily: 'POKPIX3-Regular',
        fontSize: 14,
        color: '#a20a0a50',
        width: '100%',
        textAlign: 'center'
    },
    pokeName: {
        color: '#eaebed',
        fontFamily: 'PokeClassic-Regular',
        fontSize: 24,
        marginTop: 10,
        marginBottom: 4,
        textAlign: 'center'
    },
    pokeDescription: {
        color: '#eaebed',
        fontFamily: 'PokeClassic-Regular',
        fontSize: 8,
        textAlign: 'center',
        width: '100%',
        marginBottom: 5
    },
    pokeType: {
        borderRadius: 20,
        marginHorizontal: 10,
        fontSize: 12,
        paddingHorizontal: 10,
        paddingVertical: 2,
        borderStyle: 'solid',
        borderColor: '#fff',
        borderWidth: .8,
        color: '#e8e8e8',
        fontFamily: 'PokeClassic-Regular'
    },
    typesContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical: 12,
        marginBottom: 30
    },
    shinyContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    }
})

export default Detail
