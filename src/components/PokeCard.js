import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions } from 'react-native'
import getTypeColor from '../Types'

const { width, height } = Dimensions.get('window');


const PokeCard = ({ pokemon, navigation }) => {
    return (
        <TouchableOpacity
            style={[styles.pokeCard]}
            onPress={() => navigation.navigate('Detail', {
                data: pokemon
            })}
        >
            <Image
                source={{ uri: pokemon.sprites.front_default }}
                style={styles.posterImage}
            />
            <View style={{ flex: 2 }}>
                <Text style={styles.pokeName}>{pokemon.name}</Text>
                <View style={styles.typesContainer}>
                    {pokemon.types.map(type =>
                        <View style={[styles.pokeType, { backgroundColor: getTypeColor(type.type.name) }]}>
                            <Text style={{ fontFamily: 'PokeClassic-Regular', color: 'black', fontSize: 6 }}>{type.type.name}</Text>
                        </View>
                    )}
                </View>
            </View>
            <Text style={{
                fontSize: 30,
                color: '#22283110',
                position: 'absolute',
                bottom: 0,
                right: 0,
                fontFamily: 'PokeClassic-Regular'
            }}>#{(pokemon.id / 100).toString().replace('.', '')}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    pokeCard: {
        width: width * .85,
        borderWidth: 2,
        borderStyle: 'solid',
        borderColor: '#222831',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginVertical: 8,
        borderRadius: 6,
        backgroundColor: '#e8e8e8',
        elevation: 5
    },
    pokeName: {
        color: '#222831',
        fontSize: 16,
        fontFamily: 'PokeClassic-Regular'
    },
    pokeType: {
        backgroundColor: '#30475e',
        borderRadius: 10,
        marginHorizontal: 4,
        paddingHorizontal: 8,
        borderStyle: 'solid',
        borderColor: '#fff',
        borderWidth: .8,
    },
    typesContainer: {
        flexDirection: 'row'
    },
    posterImage: {
        width: 80,
        height: 80,
        flex: 1,
        resizeMode: 'contain',
        margin: 0,
    },
})

export default PokeCard
