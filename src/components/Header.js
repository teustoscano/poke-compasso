import React from 'react'
import {
    View,
    Text,
    TouchableHighlight,
    Image,
    StyleSheet
} from 'react-native'

const Header = ({title, navigation}) => {
    return (
        <View style={styles.header}>
            <Text style={styles.title}>{title}</Text>
            <TouchableHighlight
                onPress={() => navigation.toggleDrawer()}
                underlayColor="transparent"
            >
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Image
                        source={require('../assets/images/pokeball.png')}
                        style={{ width: 40, height: 40 }}
                    />
                    <Text style={{ fontFamily: 'PokeClassic-Regular', textAlign: 'center', color: '#fff', fontSize: 8 }}>Menu</Text>
                </View>
            </TouchableHighlight>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 28,
        fontFamily: 'PokeSolid-Regular',
        color: '#eaebed',
        letterSpacing: 4
    },
    header: {
        backgroundColor: '#f05454',
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 8,
        borderBottomColor: '#222831',
        borderBottomWidth: 4,
        borderTopWidth: 4,
        borderTopColor: '#222831'
    }
})

export default Header
