import React from 'react'
import {
    View,
    Text,
    FlatList,
    Image,
    Dimensions,
    Animated,
    StyleSheet,
    ScrollView
} from 'react-native'
import Header from '../components/Header'

const { width, height } = Dimensions.get('screen')

const IMAGE_WIDTH = width * .65
const IMAGE_HEIGHT = IMAGE_WIDTH * .7
const SPACING = 20

const AnimatedFlatlist = Animated.createAnimatedComponent(FlatList)
const AnimatedView = Animated.createAnimatedComponent(View)

const DATA = [
    {
        key: 0,
        image: require('../assets/images/Kanto.png'),
        title: 'Kanto',
        subtitle: 'Red/Blue/Yellow',
        price: 151,
        info: 'All cities in Kanto are named after colors (Viridian City, Lavender Town, Indigo Plateau, etc.), with the exception of Pallet Town, which is also a reference to color.',
        cities: 10
    },
    {
        key: 1,
        image: require('../assets/images/Johto.png'),
        title: 'Johto',
        subtitle: 'Gold/Silver/Crystal',
        price: 251,
        info: 'First explored in Pokémon Gold and Silver, it is home to an additional 100 Pokémon that were not present in the previous games. Players begin their journey in New Bark Town.',
        cities: 12
    },
    {
        key: 2,
        image: require('../assets/images/Hoenn.png'),
        title: 'Hoenn',
        subtitle: 'Ruby/Sapphire/Emerald',
        price: 202,
        info: 'Hoenn is inspired by the real-world Japanese main island of Kyushu. Professor Birch of Littleroot Town offers the starter Pokémon known as Treecko, Torchic, or Mudkip.',
        cities: 16
    },
    {
        key: 4,
        image: require('../assets/images/Sinnoh.png'),
        title: 'Sinnoh',
        subtitle: 'Diamond/Pearl/Platinum',
        price: 210,
        info: 'Sinnoh is composed of the large mainland, the Battle Zone on another landmass to the northeast, and several smaller islands on both sides of the region.',
        cities: 18
    },
    {
        key: 5,
        image: require('../assets/images/Unova.png'),
        title: 'Unova',
        subtitle: 'Black/White',
        price: 155,
        info: 'It is far away from the four other large regions, and the Pokémon which inhabit Unova are diverse and different from those of Kanto, Johto, Hoenn, and Sinnoh.',
        cities: 21
    },
    {
        key: 6,
        image: require('../assets/images/Kalos.png'),
        title: 'Kalos',
        subtitle: 'X/Y',
        price: 457,
        info: 'The Kalos region is shaped like a five-pointed star, with one of its biggest cities being Lumiose City in the north-central part of the region.',
        cities: 17
    },
    {
        key: 7,
        image: require('../assets/images/Alola.png'),
        title: 'Alola',
        subtitle: 'Sun/Moon',
        price: 302,
        info: 'The Alola region is made up of four natural islands and one artificial island: Melemele Island, Akala Island, Ulaula Island, Poni Island, and Aether Paradise. It is a popular resort destination and attracts a lot of tourists from other regions.',
        cities: 10
    },
]

const Content = ({ item }) => {
    return (
        <>
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 18,
                    textTransform: 'uppercase',
                    fontFamily: 'PokeClassic-Regular',
                    color: '#222831'
                }}
            >
                {item.title}
            </Text>
            <Text style={{ fontSize: 12, opacity: .4 }}>{item.subtitle}</Text>
            <View style={{ flexDirection: 'row', marginTop: SPACING }}>
                <Text style={{ fontSize: 32, letterSpacing: 3, marginRight: 5, fontFamily: 'PokeClassic-Regular', color: '#222831' }}>{item.price}</Text>
                <Text style={{ lineHeight: 18, fontSize: 10, alignSelf: 'flex-end', fontFamily: 'PokeClassic-Regular', color: '#222831' }}>pkms</Text>
            </View>
        </>
    )
}

const Regions = ({ navigation }) => {
    const scrollX = React.useRef(new Animated.Value(0)).current;
    // get and divide screen in 0 | 0.5 | 1
    const progress = Animated.modulo(Animated.divide(scrollX, width), width)
    const [index, setIndex] = React.useState(0)
    const ref = React.useRef()

    return (
        <View style={{ backgroundColor: '#222831', flex: 1 }}>
            <Header navigation={navigation} title="Regions"/>
            <View style={{ height: IMAGE_HEIGHT * 2.1 }}>
                <AnimatedFlatlist
                    ref={ref}
                    data={DATA}
                    keyExtractor={(item) => item.key}
                    horizontal
                    pagingEnabled
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                        { useNativeDriver: true }
                    )}
                    bounces={false}
                    style={{ flexGrow: 0, zIndex: 999 }}
                    contentContainerStyle={{
                        height: IMAGE_HEIGHT + SPACING * 2,
                        paddingHorizontal: SPACING * 2,
                    }}
                    showsHorizontalScrollIndicator={false}
                    onMomentumScrollEnd={ev => {
                        setIndex(Math.floor(ev.nativeEvent.contentOffset.x / width))
                    }}
                    renderItem={({ item, index }) => {
                        const inputRange = [
                            (index - 1) * width,
                            index * width,
                            (index + 1) * width
                        ]
                        const opacity = scrollX.interpolate({
                            inputRange,
                            outputRange: [0, 1, 0]
                        })
                        const translateY = scrollX.interpolate({
                            inputRange,
                            outputRange: [50, 0, 20] //start at 50, go to 0, final in 20
                        })
                        return (
                            <AnimatedView style={{ width, paddingVertical: SPACING - 10, opacity, transform: [{ translateY }] }}>
                                <Image
                                    source={item.image}
                                    style={{ width: IMAGE_WIDTH, height: IMAGE_HEIGHT, backgroundColor: '#222831', borderRadius: 6 }}
                                />
                            </AnimatedView>
                        )
                    }}
                />

                <View style={{ width: IMAGE_WIDTH, alignItems: 'center', marginLeft: SPACING * 2, paddingHorizontal: SPACING * 2, zIndex: 99 }}>
                    {DATA.map((item, index) => {
                        const inputRange = [
                            (index - .2) * width,
                            index * width,
                            (index + .2) * width
                        ]
                        const opacity = scrollX.interpolate({
                            inputRange,
                            outputRange: [0, 1, 0],
                        })
                        const rotateY = scrollX.interpolate({
                            inputRange,
                            outputRange: ['45deg', '0deg', '45deg']
                        })

                        return (
                            <AnimatedView key={item.key} style={{ position: 'absolute', opacity, transform: [{ perspective: IMAGE_WIDTH * 4 }, { rotateY }] }}>
                                <Text style={{ position: 'absolute', right: -SPACING, top: -SPACING, fontSize: 10, letterSpacing: 3 }}>puxe ></Text>
                                <Content item={item} />
                            </AnimatedView>
                        )
                    })}
                </View>
                <AnimatedView
                    style={{
                        width: IMAGE_WIDTH + SPACING * 2,
                        position: 'absolute',
                        backgroundColor: '#eaebed',
                        zIndex: -1,
                        top: SPACING * 2,
                        left: SPACING,
                        bottom: 0,
                        shadowColor: '#000',
                        shadowOpacity: .2,
                        shadowRadius: 24,
                        shadowOffset: {
                            width: 0,
                            height: 0
                        },
                        transform: [
                            {
                                perspective: IMAGE_WIDTH * 4
                            },
                            {
                                rotateY: progress.interpolate({
                                    inputRange: [0, .5, 1],
                                    outputRange: ['0deg', '90deg', '180deg']
                                })
                            }
                        ]
                    }}
                />
            </View>

            <View>
                {DATA.map((item, index) => {
                    const inputRange = [
                        (index - .2) * width,
                        index * width,
                        (index + .2) * width
                    ]
                    const opacity = scrollX.interpolate({
                        inputRange,
                        outputRange: [0, 1, 0],
                    })

                    return (
                        <AnimatedView key={item.key} style={{ position: 'absolute', opacity, width: width * .8, alignSelf: 'center', marginVertical: SPACING }}>
                            <Text style={{ color: 'black', fontFamily: 'PokeClassic-Regular', fontSize: 14, color: '#fff', marginBottom: 5 }}>{item.cities} cities</Text>
                            <Text style={{ color: 'black', fontFamily: 'PokeClassic-Regular', fontSize: 10, color: '#fff' }}>{item.info}</Text>
                        </AnimatedView>
                    )
                })}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    loadingContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export default Regions