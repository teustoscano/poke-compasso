export default function getHabitatImage(name){
    var habitats = {
        'forest': require('./assets/images/forest.png'),
        'grassland': require('./assets/images/grassland.png'),
        'rare': require('./assets/images/rare.png'),
        'mountain': require('./assets/images/mountain.png'),
        'cave': require('./assets/images/cave.png'),
        'waters-edge': require('./assets/images/waters.png'),
        'sea': require('./assets/images/sea.png'),
        'urban': require('./assets/images/urban.png'),
        'rough-terrain': require('./assets/images/rough.png'),
    }

    return habitats[name]
}