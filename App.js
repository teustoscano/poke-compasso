/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer'
import {
  Player,
} from '@react-native-community/audio-toolkit';
import Icon from 'react-native-vector-icons/Feather';

import Home from './src/pages/Home'
import Detail from './src/pages/Detail'
import Regions from './src/pages/Regions'
import Search from './src/pages/Search'
import Generations from './src/pages/Generations'
import Animated from 'react-native-reanimated';

const AnimatedView = Animated.createAnimatedComponent(View)

const slides = [
  {
    key: 1,
    lineOne: 'c 95. 96. 98 \t\tNintendo',
    lineTwo: 'c 96. 20. 20 \t\tCompasso UOL',
    lineThree: 'c 95. 20. 20 \t\tMatheus T',
    text: '',
    img: '',
    backgroundColor: '#eaebed',
  },
  {
    key: 2,
    title: 'Poke-Agenda',
    text: 'Um projeto feito para o desafio do processo seletivo Compasso UOL.',
    img: require('./src/assets/images/ougKFey.gif'),
    backgroundColor: '#000000',
  },
  {
    key: 3,
    title: 'Poke-Compasso',
    text: 'Temos que contratar!',
    img: require('./src/assets/images/lastStep.gif'),
    backgroundColor: '#000000',
  }
];

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const p = new Player('pokeintro.mp3')

const CustomDrawerContent = (props) => {
  
  return (
    <DrawerContentScrollView>
      <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
        <View
          style={{ width: 60, height: 60, borderWidth: 2, borderStyle: 'solid', borderRadius: 30, marginRight: 10 }}
        />
        <Text style={{ fontFamily: 'PokeSolid-Regular', color: '#eaebed'}}>Poke-Compasso</Text>
      </View>
      <DrawerItem
        label="Home"
        style={styles.drawerItem}
        onPress={() => props.navigation.navigate('Home')}
        labelStyle={{ fontFamily: 'PokeClassic-Regular', color: '#222831' }}
      />
      <DrawerItem
        label="Regions"
        style={styles.drawerItem}
        onPress={() => props.navigation.navigate('Regiões')}
        labelStyle={{ fontFamily: 'PokeClassic-Regular', color: '#222831' }}
      />
      <DrawerItem
        label="Generations"
        style={styles.drawerItem}
        onPress={() => props.navigation.navigate('Gerações')}
        labelStyle={{ fontFamily: 'PokeClassic-Regular', color: '#222831' }}
      />
    </DrawerContentScrollView>
  )
}

const DrawerNavigator = ({ navigation, route }) => {
  const [progress, setProgress] = React.useState(new Animated.Value(0))

  const scale = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [0, 1]
  })
  return (
    <Drawer.Navigator
      drawerType="slide"
      overlayColor="transparent"
      drawerContentOptions={{
        activeBackgroundColor: 'transparent',
        activeTintColor: 'green',
        inactiveTintColor: 'green'
      }}
      drawerStyle={{
        backgroundColor: '#CC0000',
        borderRightWidth: 4,
        borderStyle: 'solid'
      }}
      drawerContent={(props) => {
        setProgress(props.progress);
        return <CustomDrawerContent {...props} />
      }}
    >
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="Pesquisar" component={Search} />
      <Drawer.Screen name="Regiões" component={Regions} />
      <Drawer.Screen name="Gerações" component={Generations} />
    </Drawer.Navigator>
  )
}

const renderIntro = ({ item }) => {
  return (
    <View style={[styles.introContainer, { backgroundColor: item.backgroundColor }]}>
      <Text style={styles.introLines}>{item.lineOne}</Text>
      <Text style={styles.introLines}>{item.lineTwo}</Text>
      <Text style={styles.introLines}>{item.lineThree}</Text>
      <Text style={styles.introTitle}>{item.title}</Text>
      <Text style={styles.introText}>{item.text}</Text>
      <Image
        source={item.img}
      />
    </View>
  )
}

const renderIntroButton = () => {
  return (
    <View style={styles.introButton}>
      <Icon
        name="arrow-right"
        size={22}
        color="#f05454"
      />
    </View>
  )
}

const renderDoneButton = () => {
  return (
    <View style={styles.introButton}>
      <Icon
        name="check"
        size={22}
        color="#f05454"
      />
    </View>
  )
}

const App: () => React$Node = () => {
  const [showApp, setShowApp] = React.useState(false)

  const whenDone = () => {
    // p.destroy()
    p.volume = 0.2
    p.looping = true
    p.play()
    setShowApp(true)
  }
  return (
    showApp === true ?
      <NavigationContainer>
        <AnimatedView style={{flex: 1, }}>
          <Stack.Navigator initialRouteName="Home" screenOptions={{
            headerTransparent: true
          }}>
            <Stack.Screen
              name="Home"
              component={DrawerNavigator}
              options={{ title: 'Welcome', headerShown: false }}
            />
            <Stack.Screen
              name="Detail"
              component={Detail}
              options={{ title: 'Pokemon', headerShown: false }}
            />
          </Stack.Navigator>
        </AnimatedView>
      </NavigationContainer> :
      <AppIntroSlider
        renderItem={renderIntro}
        data={slides}
        onDone={() => whenDone()}
        renderNextButton={renderIntroButton}
        renderDoneButton={renderDoneButton}
      />
  );
};

const styles = StyleSheet.create({
  introLines: {
    fontFamily: 'PokeClassic-Regular',
    width: '90%',
    fontSize: 14,
    color: '#2d2a32'
  },
  introContainer: {
    flex: 1,
    backgroundColor: '#eaebed',
    alignItems: 'center',
    justifyContent: 'center'
  },
  introTitle: {
    color: '#FFDE00',
    fontFamily: 'PokeSolid-Regular',
    fontSize: 44
  },
  introText: {
    color: '#eaebed',
    fontFamily: 'PokeClassic-Regular',
    fontSize: 15,
    textAlign: 'center',
    width: '80%'
  },
  introButton: {
    width: 60,
    height: 60,
    backgroundColor: '#fff',
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  drawerItem: {
    borderWidth: 2,
    borderStyle: 'solid',
    backgroundColor: '#eaebed',
    borderColor: '#fff',
  }
})

export default App;
