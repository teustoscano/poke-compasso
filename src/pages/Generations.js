import React from 'react'
import PokePosters from '../components/PokePosters'
import Header from '../components/Header'

const Generations = ({ navigation }) => {
    return (
        <>
            <Header navigation={navigation} title="Generations"/>
            <PokePosters />
        </>
    )
}
export default Generations

