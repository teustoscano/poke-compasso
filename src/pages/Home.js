import React from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TouchableHighlight
} from 'react-native'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import PokeCard from '../components/PokeCard'
import Header from '../components/Header'

const { width, height } = Dimensions.get('window');

const Home = ({ navigation }) => {
    const [pokemons, setPokemons] = React.useState([])
    const [nextPokemons, setNextPokemons] = React.useState('')
    const [previousPokemons, setPrevioustPokemons] = React.useState()
    const [pokeWithDetails, setPokeWithDetails] = React.useState([])
    const [link, setLink] = React.useState('https://pokeapi.co/api/v2/pokemon')

    React.useEffect(() => {
        let pokeArray = []
        let pokeN = ''
        let pokeP = ''
        async function fetchPokemons() {
            fetch(link)
                .then(response => response.json())
                .then(json => {
                    // console.log(json)
                    pokeN = json.next
                    pokeP = json.previous
                    const jsonPromises = json.results.map(el => {
                        return fetch(el.url).then(response => response.json())
                    })
                    return Promise.all(jsonPromises)
                }).then(arr => {
                    setNextPokemons(pokeN)
                    setPrevioustPokemons(pokeP)
                    for (const [index, json] of arr.entries()) {
                        // console.log(json)
                        pokeArray.push(json)
                        setPokeWithDetails(pokeArray)
                    }
                })
        }
        fetchPokemons()
    }, [link])

    const handlePagination = (newLink) => {
        setLink(newLink)
    }

    return (
        <View style={styles.container}>
            <Header navigation={navigation} title="Home"/>
            <Text style={styles.paragraph}>
                The pokedex is a digital encyclopedia created by Professor Oak as an invaluable tool to Trainers.
            </Text>

            <View style={styles.containerPagination}>
                <TouchableOpacity
                    style={(previousPokemons === null) ? styles.disabledBtn : styles.paginationBtn}
                    disabled={previousPokemons === null}
                    onPress={() => handlePagination(previousPokemons)}
                >
                    <Text style={{ color: '#eaebed', fontFamily: 'PokeClassic-Regular', fontSize: 8 }}>Previous</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.paginationBtn}
                    onPress={() => handlePagination(nextPokemons)}
                >
                    <Text style={{ color: '#eaebed', fontFamily: 'PokeClassic-Regular', fontSize: 8 }}>Next</Text>
                </TouchableOpacity>
            </View>

            <FlatList
                data={pokeWithDetails}
                extraData={nextPokemons}
                keyExtractor={(pokemon) => pokemon.name}
                bounces={false}
                // horizontal
                contentContainerStyle={{ alignItems: 'center' }}
                renderItem={({ item, index }) => {
                    return (
                        <PokeCard pokemon={item} navigation={navigation} />
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        flex: 1,
        alignItems: 'center',
        padding: 0
    },
    paragraph: {
        fontSize: 10,
        color: 'gray',
        textAlign: 'center',
        width: '90%',
        marginVertical: 14,
        fontFamily: 'PokeClassic-Regular'
    },
    loadingContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerPagination: {
        width: width * .8,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingVertical: 7
    },
    paginationBtn: {
        paddingHorizontal: 12,
        paddingVertical: 6,
        borderStyle: 'solid',
        borderColor: '#222831',
        borderWidth: 2,
        borderRadius: 10,
        backgroundColor: '#30475e'
    },
})

export default Home