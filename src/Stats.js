export default function getStatsColor(name){
    var stats = {
        'hp': '#fd3a69',
        'attack': '#99f3bd',
        'defense': '#19d3da',
        'special-attack': '#a37eba',
        'special-defense': '#64958f',
        'speed': '#f8bd7f',
    }

    return stats[name]
}