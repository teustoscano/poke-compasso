export default function getTypeColor(pokeType){
    var types = {
        'ground': '#af6b58',
        'fire': '#ff8e71',
        'water': '#aee6e6',
        'rock': '#734046',
        'ice': '#d9e4dd',
        'flying': '#d0e8f2',
        'steel': '#bbbfca',
        'dark': '#393e46',
        'electric': '#ffdd93',
        'psychic': '#9088d4',
        'grass': '#c6ebc9',
        'normal': '#ffe0d8',
        'poison': '#da9ff9',
        'bug': '#bedbbb',
        'fighting': '#aaaaaa',
        'ghost': '#892cdc',
        'fairy': '#ea86b6'
    }

    return types[pokeType]
}