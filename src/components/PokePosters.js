import React from 'react'
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    Dimensions,
    Image,
    FlatList,
    Animated
} from 'react-native'
import {
    State,
    Directions,
    FlingGestureHandler,
    ScrollView
} from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/EvilIcons';

const { width, height } = Dimensions.get('screen')
const AnimatedFlatlist = Animated.createAnimatedComponent(FlatList)
const AnimatedView = Animated.createAnimatedComponent(View)

const DATA = [
    {
        title: 'Generation I',
        location: 'Kanto',
        date: 'Feb 27th, 1996',
        poster: require('../assets/images/gen1.jpg')
    },
    {
        title: 'Generation II',
        location: 'Johto',
        date: 'Nov 21th, 1999',
        poster: require('../assets/images/gen2.jpg')
    },
    {
        title: 'Generation III',
        location: 'Hoenn',
        date: 'Nov 21th, 2002',
        poster: require('../assets/images/gen3.png')
    },
    {
        title: 'Generation IV',
        location: 'Sinnoh',
        date: 'Sep 28th, 2007',
        poster: require('../assets/images/gen4.jpg')
    },
    {
        title: 'Generation V',
        location: 'Unova',
        date: 'Sep 18th, 2010',
        poster: require('../assets/images/gen5.png')
    },
    {
        title: 'Generation VI',
        location: 'Kalos',
        date: 'Oct 18th, 2013',
        poster: require('../assets/images/gen6.png')
    },
    {
        title: 'Generation VII',
        location: 'Alola',
        date: 'Nov 18th, 2016',
        poster: require('../assets/images/gen7.png')
    },
    {
        title: 'Generation VIII',
        location: 'Galar',
        date: 'Nov 15th, 2019',
        poster: require('../assets/images/gen8.png')
    },
]

const OVERFLOW_HEIGHT = 70;
const SPACING = 10;
const ITEM_WIDTH = width * .72;
const ITEM_HEIGHT = ITEM_WIDTH * 1.7;
const VISIBLE_ITEMS = 3

const OverflowItems = ({ data, scrollXAnimated }) => {
    const inputRange = [-1, 0, 1]
    const translateY = scrollXAnimated.interpolate({
        inputRange,
        outputRange: [OVERFLOW_HEIGHT, 0, -OVERFLOW_HEIGHT]
    })

    return (
        <View style={style.overflowContainer}>
            <AnimatedView style={{ transform: [{ translateY }] }}>
                {data.map((item, index) => {
                    return (
                        <View key={index} style={style.itemContainer}>
                            <Text style={[style.title]} numberOfLines={1}>
                                {item.title}
                            </Text>
                            <View style={style.itemContainerRow}>
                                <Text style={[style.location]}>
                                    <Icon
                                        color='black'
                                        size={16}
                                        name="location"
                                        style={{ marginRight: 5 }}
                                    />
                                    {item.location}
                                </Text>
                                <Text style={[style.date]}>{item.date}</Text>
                            </View>
                        </View>
                    )
                })}
            </AnimatedView>
        </View>
    )
}

const PokePosters = () => {
    const [data, setData] = React.useState(DATA)
    const scrollXIndex = React.useRef(new Animated.Value(0)).current
    const scrollXAnimated = React.useRef(new Animated.Value(0)).current
    const [index, setIndex] = React.useState(0)
    const setActiveIndex = React.useCallback((activeIndex) => {
        setIndex(activeIndex);
        scrollXIndex.setValue(activeIndex);
    })

    React.useEffect(() => {
        if (index === data.length - VISIBLE_ITEMS) {
            //get new data
            const newData = [...data, ...data]
            setData(newData)
        }
    })

    React.useEffect(() => {
        Animated.spring(scrollXAnimated, {
            toValue: scrollXIndex,
            useNativeDriver: true,
        }).start()

        // AUTOMATIC SLIDER
        // setInterval(() => {
        //     scrollXIndex.setValue(Math.floor(Math.random() * data.length))
        // }, 3000);
    })

    return (
        <FlingGestureHandler
            key='left'
            direction={Directions.LEFT}
            onHandlerStateChange={ev => {
                if (ev.nativeEvent.state === State.END) {
                    if (index === data.length - 1) {
                        return
                    }
                    setActiveIndex(index + 1)
                }
            }}
        >
            <FlingGestureHandler
                key='right'
                direction={Directions.RIGHT}
                onHandlerStateChange={ev => {
                    if (ev.nativeEvent.state === State.END) {
                        if (index === 0) {
                            return
                        }
                        setActiveIndex(index - 1)
                    }
                }}
            >
                <SafeAreaView style={style.container}>
                    <OverflowItems data={data} scrollXAnimated={scrollXAnimated} />
                    <FlatList
                        data={data}
                        horizontal
                        inverted
                        contentContainerStyle={{
                            flex: 1,
                            justifyContent: 'center',
                            padding: SPACING * 2,
                        }}
                        scrollEnabled={false}
                        removeClippedSubviews={false}
                        CellRendererComponent={({ item, index, children, style, ...props }) => {
                            const newStyle = [style, { zIndex: data.length - index }];
                            return (
                                <View style={newStyle} index={index} {...props}>
                                    {children}
                                </View>
                            )
                        }}
                        keyExtractor={(_, index) => String(index)}
                        renderItem={({ item, index }) => {
                            const inputRange = [
                                index - 1,
                                index,
                                index + 1
                            ]
                            const translateX = scrollXAnimated.interpolate({
                                inputRange,
                                outputRange: [50, 0, -100]
                            })
                            const scale = scrollXAnimated.interpolate({
                                inputRange,
                                outputRange: [.8, 1, 1.3]
                            })
                            const opacity = scrollXAnimated.interpolate({
                                inputRange,
                                outputRange: [1 - 1 / VISIBLE_ITEMS, 1, 0]
                            })

                            return (
                                <AnimatedView
                                    style={{
                                        position: 'absolute',
                                        left: -ITEM_WIDTH / 2,
                                        opacity,
                                        transform: [{ translateX }, { scale }],
                                    }}
                                >
                                    <Image
                                        style={{
                                            width: ITEM_WIDTH,
                                            height: ITEM_HEIGHT,
                                            resizeMode: 'contain',
                                            borderRadius: 4,
                                        }}
                                        source={item.poster}
                                    />
                                </AnimatedView>
                            )
                        }}
                    />
                </SafeAreaView>
            </FlingGestureHandler>
        </FlingGestureHandler>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    title: {
        fontSize: 18,
        letterSpacing: -1,
        textTransform: 'uppercase',
        fontFamily: 'PokeClassic-Regular'
    },
    location: {
        fontSize: 12,
        fontFamily: 'PokeClassic-Regular'
    },
    date: {
        fontSize: 10,
        fontFamily: 'PokeClassic-Regular'
    },
    itemContainer: {
        height: OVERFLOW_HEIGHT,
        padding: SPACING,
        borderStyle: 'solid',
        borderWidth: .6,
    },
    itemContainerRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    overflowContainer: {
        height: OVERFLOW_HEIGHT,
        overflow: 'hidden',
        borderStyle: 'solid',
        borderWidth: 2,
        margin: 5,
        borderRadius: 2,
        padding: 1,
        backgroundColor: '#eaebed'
    }
})

export default PokePosters
